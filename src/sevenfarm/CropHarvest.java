package sevenfarm;

/*
 * This class handles everything todo with crop harvesting and handing out the drops depending on
 * several varribles!
 */

import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;

public class CropHarvest implements org.bukkit.event.Listener {
  
  @EventHandler
  public void onHarvest(BlockBreakEvent event)
  {
    if (event.isCancelled()) { return;
    }
    
    // Ints used for determining drops
    int seedDrop = 0;
    int cropDrop = 0;
    int enchBonus = 0;
    
    Block block = event.getBlock();
    Material blocktype = block.getType();
    
    ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
    Material itemtype = item.getType();
    
    MaterialData blockdata = block.getState().getData();
     
    // Determines Itemtype and assigns values accordingly
    switch (itemtype) {
        case WOOD_HOE: case STONE_HOE:
            seedDrop = 1;
    	cropDrop = 1;
      break;

        case IRON_HOE: case GOLD_HOE:
            seedDrop = 1;
    	cropDrop = 2;
      break;

        case DIAMOND_HOE:
    	seedDrop = 1;
    	cropDrop = 3;
      break;
      
     default:
    	 break;
    }
    
    // Checks if the tool is enchanted with forture (Probably can be done cleaner in a single Switch Statement wasn't sure how to get it to work but it's on my todo)
    
    if(item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 1){
    	enchBonus = 1;
    }
    
    if(item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 2){
    	enchBonus = 2;
    }
    
    if(item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 3){
    	enchBonus = 3;
    }
    
    // Crop type Checker and drop distrubtor!
   
    switch (blocktype) {
    case CROPS: 
    	event.getBlock().setType(Material.AIR);
    	if (((Crops) blockdata).getState() == CropState.RIPE){
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.WHEAT, cropDrop + enchBonus));
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SEEDS, seedDrop + enchBonus));
      }
      break;
    
    case POTATO: 
    	event.getBlock().setType(Material.AIR);
    	if (((Crops) blockdata).getState() == CropState.RIPE){
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.POTATO_ITEM, cropDrop + enchBonus));
      }
      break;
    
    case CARROT: 
    	event.getBlock().setType(Material.AIR);
    	if (((Crops) blockdata).getState() == CropState.RIPE){
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CARROT_ITEM, cropDrop + enchBonus));
      }
      break;
    
    case COCOA: 
    	event.getBlock().setType(Material.AIR);
        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.INK_SACK, cropDrop + enchBonus, (short)3));
      break;
      
    case SUGAR_CANE_BLOCK: 
    	event.getBlock().setType(Material.AIR);
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SUGAR_CANE, cropDrop + enchBonus));
      break;
    
    case CACTUS: 
    	event.getBlock().setType(Material.AIR);
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CACTUS, cropDrop + enchBonus));
      break;
    
    case MELON_BLOCK: 
    	event.getBlock().setType(Material.AIR);
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.MELON, cropDrop + enchBonus));
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.MELON_SEEDS, seedDrop + enchBonus));
      break;
    
    case PUMPKIN: 
    	event.getBlock().setType(Material.AIR);
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.PUMPKIN, cropDrop + enchBonus));
    	event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.PUMPKIN_SEEDS, seedDrop + enchBonus));
      break; 
    
    case BEETROOT_BLOCK: 
    	event.getBlock().setType(Material.AIR);
    	if (((Crops) blockdata).getState() == CropState.RIPE){
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BEETROOT, cropDrop + enchBonus));
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BEETROOT_SEEDS, seedDrop + enchBonus));
      }
      break;
    
    case NETHER_STALK: 
    	event.getBlock().setType(Material.AIR);
    	if (((Crops) blockdata).getState() == CropState.RIPE){
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.NETHER_WARTS, cropDrop + enchBonus));
      }
      break;
	
    default:
		break;
    }
  }
}
