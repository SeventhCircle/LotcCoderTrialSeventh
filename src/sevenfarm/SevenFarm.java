package sevenfarm;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File
/*
 * This is the main class of 7farm and I specifically designed it to be as simple and concise as I could with
 * my semi limited knowledge but only the most necessary things were done here.
 */

public class SevenFarm extends JavaPlugin {
    public static SevenFarm plugin;


    public void onEnable() {
        plugin = this;
        // Logging File
        if ( boolean exists ("settingsplayer")){

        }

        // Config Defaults
        getConfig().addDefault("DenyGlobalSoilTrample", false);
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();

        // Command Registry
        getCommand("7fglobalprotect").setExecutor(new CropTrampleCommand());
        getCommand("7fglobalconfigcheck").setExecutor(new CropTrampleCommand());

        // Event Registry
        getServer().getPluginManager().registerEvents(new CropTrampleEvent(), this);
        getServer().getPluginManager().registerEvents(new CropHarvest(), this);
    }

    public void onReload() {
        saveConfig();
    }

    public void onDisable() {
        saveConfig();
        plugin = null;
    }
}
