package sevenfarm;

/*
 * This class is responsilbe for watching for the block interaction that causes soil to be turned into
 * dirt and act on it accordingly to the config settings which this class also checks each time the command is issued 
 * to ensure always upto date info! 
 */


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Scanner;

public class CropTrampleEvent implements Listener {
    private FileConfiguration getconfig = SevenFarm.plugin.getConfig();
   
  //Event designed to handle the soil trample protection
  @EventHandler(priority = normal, ignoreCancelled = true)
  public void onTrample(PlayerInteractEvent event) {

      //config check for global setting
      if (getconfig.getBoolean("denyGlobalSoilTrample", true)) {
          if (event.getAction() == Action.PHYSICAL) {
              Block block = event.getClickedBlock();
        
    		if (block == null) {
    			return;
    		}
        
    		Material blockType = block.getType();

              //Checks to ensure that the event in question is in fact soil being trampled
              if (blockType == Material.SOIL) {
    			event.setUseInteractedBlock(Event.Result.DENY); 
    			event.setCancelled(true);
    			block.setType(blockType);
        }
          } else { //config check for player specific setting
              Player player = event.getPlayer();
              Scanner settingplayer = new Scanner("settingsplayer");

              while (settingplayer.hasNextLine()) {
                  if (player.equals(settingplayer.nextLine().trim())) {

                  } else break;
              }
          }
    }
  }
}
