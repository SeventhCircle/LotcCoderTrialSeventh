package sevenfarm;

/* 
 * This is the Command Issuing Class I figured it was simplest to have one central class that is looking for
 * commands verse several all looking for the same imput. I'm not 100% sure if this is the best way but I felt it 
 * made the most sense.
*/

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

public class CropTrampleCommand implements org.bukkit.command.CommandExecutor {

    private FileConfiguration getconfig = SevenFarm.plugin.getConfig();

    // The Main CommandSender Method
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        // 7protect Command for stopping soil trample
        if (commandLabel.equalsIgnoreCase("7fglobalprotect")) {
            if (sender.hasPermission("7farm.globalprotect")) {
        Boolean bargs = Boolean.valueOf(args[0]);
                getconfig.set("denySoilTrample", bargs);
        sender.sendMessage(ChatColor.AQUA + "Deny Soil Trample has been set to: " + ChatColor.BOLD + ChatColor.GRAY + bargs);
        return true;
      }else{
          sender.sendMessage(ChatColor.DARK_RED + "You lack permission to issue this command: " + ChatColor.GRAY + commandLabel);
          }
    }
    
    // Config checking command was used for debugging but decided not to remove it because it could be handy
        if (commandLabel.equalsIgnoreCase("7fglobalconfigcheck")) {
            if (sender.hasPermission("7farm.globalconfigedit")) {
                sender.sendMessage(ChatColor.AQUA + "Config File Shows Deny Soil Trample as" + ChatColor.BOLD + ChatColor.GRAY + getconfig.getBoolean("denySoilTrample"));
        return true;
      }else{
      sender.sendMessage(ChatColor.DARK_RED + "You lack permission to issue this command: " + ChatColor.GRAY + commandLabel);
      }
    }
    
    return false;
  }
}
